var d = document
var b = d.querySelector('body')
var started = false
var startDelay = "0:2:0"
var snareStartDelay = "0:3:0"

/*
 KICK
 */
var kick = new Tone.MembraneSynth({
  "envelope" : {
    "attack" : 0.01,
    "attackCurve" :  "exponential",
    "decay" : 0.47,
    "sustain" : 0.1,
    "release" : 1.4,
  },
  "volume": 2,
  "octaves" : 5.1,
}).toMaster();

var kickPart = new Tone.Part(function(time, note){
  kick.triggerAttackRelease(note, "12n", time);
},[["0:0:0", "C2"], ["0:1.5:0", "C2"], ["0:2:0", "C2"]]).start(startDelay);
kickPart.loop = true

r6.onchange = function(){kick.pitchDecay = this.value}


/*
 SNARE
 */
const pingPongForSnare = new Tone.PingPongDelay(0, 0).toMaster();

var snareFiltered = new Tone.NoiseSynth({
  "volume" : 7,
  "noise":{
    "type":"white",
    "playbackRate": 4,
  },
  "envelope" : {
    "attack" : 0.01,
    "sustain" : 0,
    "decay" : 0.23,
  },
}).connect(pingPongForSnare);
var snare = new Tone.NoiseSynth({
  "volume" : 7,
  "noise":{
    "type":"white",
    "playbackRate": 4,
  },
  "envelope" : {
    "attack" : 0.01,
    "sustain" : 0,
    "decay" : 0.23,
  },
}).toMaster();



r3.onchange = function(){snare.envelope.decay = this.value / 10}
r4.onchange = function(){
  pingPongForSnare.delayTime.value = this.value / 10
  pingPongForSnare.feedback.value = this.value
}

var snarePart = new Tone.Loop(function(time){
  snareFiltered.triggerAttack(time);
  snare.triggerAttack(time);
}, "2n").start(snareStartDelay);

// Drumfill, intro only
var stateNbr = 0
var drumfillIntro = new Tone.Sequence(function(time, note){
  snare.triggerAttack(time);
  if (started == false) {
    Tone.Draw.schedule(function(){
      stateNbr++
      let = state = b.getAttribute('data-state')
      b.setAttribute('data-state', state + stateNbr +" ")
      d.getElementById('g'+stateNbr).classList.add('visible')
      if (stateNbr >= 8) {
        started = true
        stateNbr = 0
      }
    }, time)
  }
},[["C2","C2","C2","C2"],["C2","C2","C2","C2"]]).start(0);
drumfillIntro.loop = 1

// Drumfill to trigger
var dfDelay = startDelay * 2
var drumfill = new Tone.Sequence(function(time, note){
  snare.triggerAttack(time);
},[["C2","C2","C2","C2"],["C2","C2","C2","C2"]]).start(0);
drumfill.loop = true
drumfill.mute = true


/**
 *  HIHAT
 */
var hihat = new Tone.NoiseSynth({
  "volume" : -15,
  "noise":{
    "playbackRate": 4,
  },
  "envelope" : {
    "attack" : 0.01,
    "decay" : 0.12,
    "sustain" : 0,
  },
  "filterEnvelope" : {
    "attack" : 0.001,
    "decay" : 0.01,
    "sustain" : 0,
    "release" : 0.44
  }
}).toMaster();


var hihatPart = new Tone.Loop(function(time){
  hihat.triggerAttack(time);
}, "8n").start(startDelay);

/**
 *  SLENG TENG PIANO
 */
const pingPongForPiano = new Tone.PingPongDelay(0, 0).toMaster();

var piano = new Tone.PolySynth(4, Tone.Synth, {
  "volume" : -6,
  "oscillator" : {
    "type" : "custom",
    "partials" : [1, 2, 1, 1, 2, 1],
  },
  "envelope" : {
    "release" : 0.20,
  },
  "portamento" : 0.05
}).toMaster();
var pianoFiltered = new Tone.PolySynth(4, Tone.Synth, {
  "volume" : -8,
  "oscillator" : {
    "type" : "custom",
    "partials" : [1, 2, 1, 1, 2, 1],
  },
  "envelope" : {
    "release" : 0.20,
  },
  "portamento" : 0.05
}).connect(pingPongForPiano);

r5.onchange = function(){
  pingPongForPiano.delayTime.value = this.value 
  pingPongForPiano.feedback.value = this.value
}

var c1 = ["D4", "F#4", "A4"];
var c2 = ["C4", "E4", "G4"];

var pianoPart = new Tone.Part(function(time, chord){
  pianoFiltered.triggerAttackRelease(chord, "32n", time);
  piano.triggerAttackRelease(chord, "32n", time);
}, [["0:0:2", c1],["0:1:2", c1], ["0:2:2", c1], ["0:3:2", c2]]).start(startDelay);

pianoPart.loop = true;
pianoPart.humanize = true;

/**
 *  Stalag Brass
 */
var freeverb = new Tone.Freeverb().toMaster();
freeverb.dampening.value = 8000;
var brass = new Tone.PolySynth(4, Tone.Synth, {
  "volume" : -15,
  "oscillator" : {
    "type" : "sawtooth",
    "partials" : [1, 3, 0, 2, 1],
  },
  "envelope" : {
    "attack" : 0.03,
    "release" : 0.40,
    "sustain" : 0.50,
    "decay" : 0.10,
  },
}).toMaster();

brass.connect(freeverb)
var h1 = ["D3"];
var h2 = ["E3", "C4"];

var brassPart = new Tone.Part(function(time, note){
  brass.triggerAttackRelease(note.noteName, note.duration, time);
}, [
  {"time":"0:0:0","noteName":h1, "duration":"16n"},
  {"time":"0:0:2","noteName":h1, "duration":"20n"},
  {"time":"0:2:0.1","noteName":h2, "duration":"7n"},
  {"time":"0:3:0.1","noteName":h2, "duration":"7n"}

]).start(startDelay);
brassPart.mute = true
brassPart.loop = true;
brassPart.humanize = false;


/*
 BASS
 */
var bass = new Tone.MonoSynth({
  "volume" : -0,
  "envelope" : {
    "attack" : 0.03,
    "decay" : 0.7,
    "sustain" : 0.06,
    "release" : 0.18,
    "releaseCurve" : "linear",
  },
  "oscillator" : {
     "type" : "pulse",
     "width" : 0.1 // tweak
  },
  "filter" :{
    "Q" : 0 ,
    "type" : "lowpass" ,
    "rolloff" : -12
  },
  "filterEnvelope" : {
    "attack" : 0.001,
    "decay" : 0.03,
    "sustain" : 0.5,
    "baseFrequency" : 200, // tweak
    "octaves" : 2.6
  }
}).toMaster();


var bassPart = new Tone.Part(function(time, note){
  bass.triggerAttackRelease(note, "32n", time);
}, [
  ["0:0:0","C#2"],["0:0:1","D2"],["0:0:2","D2"],["0:0:3","D2"],
  ["0:1:0","C#2"],["0:1:1","D2"],["0:1:2","D2"],["0:1:3","D2"],
  ["0:2:0","C#2"],["0:2:1","D2"],["0:2:2","D2"],["0:2:3","D2"],
  ["0:3:0","G2"],["0:3:1.9","F#2"]
]).start(startDelay);
bassPart.loop = true;

r1.onchange = function(){bass.filterEnvelope.baseFrequency = this.value}
r2.onchange = function(){bass.oscillator.width.value = this.value;}

//set the transport
Tone.Transport.bpm.value = 80;

function setToggle(btnClass, part){
  var btn = d.querySelector('.'+btnClass)
  btn.onclick = function(){
    togglePart(btnClass, part)
  }
}

function togglePart(btnClass, part){
  var btn = d.querySelector('.'+btnClass)
  if (part.mute) {
    btn.classList.add('playing')
    part.mute = false
  }else {
    btn.classList.remove('playing')
    part.mute = true
  }
}

document.onkeydown= function(e){
  if (e.key == 0) {togglePlay()}
  if (e.key == 1) {togglePart('stalag-toggle',brassPart)}
  if (e.key == 2) {togglePart('drumfill-toggle',drumfill)}
  if (e.key == 3) {togglePart('bass-toggle',bassPart)}
  if (e.key == 4) {togglePart('kick-toggle',kickPart)}
  if (e.key == 5) {togglePart('piano-toggle',pianoPart)}
}

setToggle('stalag-toggle', brassPart)
setToggle('drumfill-toggle', drumfill)
setToggle('bass-toggle', bassPart)
setToggle('kick-toggle', kickPart)
setToggle('piano-toggle', pianoPart)

// Start button
var startBtn = document.querySelector('.start')
startBtn.addEventListener('click', () => togglePlay())

function togglePlay(){
  if (started) {
    Tone.Transport.stop()
    started = false
    // startBtn.innerHTML = ">"
    startBtn.classList.remove('playing')
  }else {
    Tone.Transport.start()
    startBtn.classList.add('playing')
    // started = true // see intro
    // startBtn.innerHTML = "||"
  }
}

// Cosmetics

let sizes = ['small','medium','large']
let cols = ['palegreen', 'purple', 'tomato', 'tan']
let borders = ['solid', 'groove', 'double', 'ridge', 'outset']
let els = d.querySelectorAll('.btn') 
// els.forEach(el => {
//   genStyle(el)
// });

function genStyle(el){
  bgi = Math.round(Math.random() * 11)
  t = Math.round(Math.random() * 100)
  l = Math.round(Math.random() * 100)
  c1 = Math.floor(Math.random() * cols.length)
  c2 = Math.floor(Math.random() * cols.length)
  b1 = Math.floor(Math.random() * borders.length)
  s = Math.floor(Math.random() * sizes.length)
  col = cols[c1]
  bcol = cols[c2]
  border = borders[b1]
  el.setAttribute('style','background:'+col+' url(img/'+bgi+'.png); border:'+l+'px '+border+' '+bcol+';')
}
