let container = document.querySelector('main');
let rectangle = document.createElement('div');
rectangle.classList.add("rectangle1");
rectangle.classList.add("box");
container.appendChild(rectangle);
//
// let rectangle1 = document.createElement('div');
// rectangle1.classList.add("rectangle1");
// document.body.appendChild(rectangle1);


let animation = anime({
  targets: rectangle,
  skewX: 50,
  skewY: 10,
  loop: true,
  direction: 'alternate',
  duration: 1000,
  autoplay: false,
  easing: 'easeInOutSine'
});


rectangle.addEventListener('mouseenter', function(ev) {
  animation.play();
})
rectangle.addEventListener('mouseleave', function(ev) {
  animation.pause();
})




function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}
