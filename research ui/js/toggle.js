let switchable = ["coccus.ico", "s-1.ico", "spirochete.ico", "s-10.ico", "BlingRing-Blue-S.ico"];
let touchEvent = 'ontouchstart' in window ? 'touchstart' : 'click';
let nbOfToggle = 5;
let toggleArray = [];

createToggle(nbOfToggle);

function createToggle(nbOfToggle) {
  for (let i = 0; i < nbOfToggle; i++) {
    let img = document.createElement('img');
    img.classList.add("switchable");
    img.classList.add("box");
    img.src = "img/toggle/" + switchable[i];
    toggleArray.push(img)
    container.appendChild(img);
  }
}

toggleArray[0].addEventListener(touchEvent, function() {
  this.classList.toggle("mirror");
  this.classList.toggle("gray");

})


toggleArray[1].addEventListener(touchEvent, function() {
  this.classList.toggle("rotation");
})


toggleArray[2].addEventListener(touchEvent, function() {
  this.classList.toggle("switch2");
})



toggleArray[3].addEventListener(touchEvent, function() {
  this.classList.toggle("switch3");
})


toggleArray[4].addEventListener(touchEvent, function() {
  this.classList.toggle("switch4");
})
