console.log("here");


let numberOfDivs = 39;
let rectangleArray = [];


(function main() {


  for (var i = 0; i < numberOfDivs; i++) {
    let div = document.createElement('div');
    div.classList.add("rectangleMultiples");
    div.classList.add("box");
    div.style.width = getRandomInt(20, 250) + "px";
    div.style.height = getRandomInt(20, 250) + "px";

    let animation = anime({
      targets: div,
      skewX: getRandomInt(-50, 50),
      skewY: getRandomInt(-50, 50),
      loop: true,
      direction: 'alternate',
      duration: 1000,
      autoplay: false,
      easing: 'easeInOutSine'
    });
    // div.style.transformeskewX = "25deg";

    div.style.transform = ("skewX(" + getRandomInt(-50, 50) + "deg ) skewY(" + getRandomInt(-50, 50) + "deg )");


    div.animation = animation;
    div.addEventListener('mouseenter', function(ev) {
      div.animation.play();
    })
    div.addEventListener('mouseleave', function(ev) {
      div.animation.pause();
      div.animation = anime({
        targets: div,
        skewX: getRandomInt(-50, 50),
        skewY: getRandomInt(-50, 50),
        loop: true,
        direction: 'alternate',
        duration: 1000,
        autoplay: false,
        easing: 'easeInOutSine'
      });
    })

    container.appendChild(div);
    rectangleArray.push(div);
  }
})()









function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}
